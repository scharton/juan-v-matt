package juan.v.matt;

public class ReportRow {

    private ReportContact nameLink;

    public ReportContact getNameLink() {
        return nameLink;
    }

    public void setNameLink(ReportContact nameLink) {
        this.nameLink = nameLink;
    }

}
